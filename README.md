# RestApi Server
Simple REST api server created with symfony and api platform (https://api-platform.com)
## Requirements

- PHP >= 7.1
- Symfony 4.4
- SQL database

## Install via composer
Clone repo then run

    composer require mspyra/server
Import routing in **config/routes/annotations.yaml**

    mspyra_server_controllers:  
		resource: '@MspyraServerBundle/Controller/'  
		type: annotation

Set **DATABASE_URL** in your **.env** file then run

     php bin/console doctrine:database:create
     php bin/console doctrine:schema:update

## Usage

Start web server and navigate to https://localhost in your browser
You can also use other tools e.g. Postman
`https://localhost/api/items`



