<?php

namespace Mspyra\Server\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="mspyra_server_index")
     */
    public function index(): RedirectResponse
    {
        return $this->redirectToRoute('api_entrypoint');
    }
}